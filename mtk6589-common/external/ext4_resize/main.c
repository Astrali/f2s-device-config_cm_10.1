#include <utils/Log.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include "ext2fs/ext2fs.h"

#define LOG_TAG "Resize"
#define LK_FILE_MAX_SIZE (16 * 1024)
#define STATS_RESIZED 1
#define DATA_PATH "/emmc@usrdata" 
#define RESERVE_SIZE (1 * 1024 * 1024)

//Ioctl for lk_env
#define ENV_MAGIC	 'e'
#define ENV_READ		_IOW(ENV_MAGIC, 1, int)
#define ENV_WRITE 		_IOW(ENV_MAGIC, 2, int)
#define BUF_MAX_LEN 20
#define ITEM_NAME   "resized_stats"

struct env_ioctl
{
	char *name;
	int name_len;
	char *value;
	int value_len;	
};

static int process_system_env(int *resize_stats);
static int set_system_env(const char *status);
static unsigned long long get_block_size(const char *path);
static int get_resize_stats(int *resize_stats);

int main(int argc, char* agrv[])
{
    int resize_stats = 0;
    int result = 0;
    if ((result = get_resize_stats(&resize_stats)) < 0) {
        ALOGE("process system env fail.\n");
        return -1;
    }

    if (STATS_RESIZED == resize_stats) {
        ALOGI("Partition has been resized,so nothing to do!\n");
        return 0;
    }else {
        //Get block size of data partition.
        pid_t pid;
        int status;
        int child_return_val;
        unsigned long long data_partition_size = 0;
        char size[8]; // data_partition max size is 999999M
        int ret;
        int resize_ret = 0;

        if((data_partition_size = get_block_size(DATA_PATH)) < 0) {
            ALOGE("Get data partition size fail.\n");
            return -1;
        }
        data_partition_size -= RESERVE_SIZE;
        
        if((ret = sprintf(size, "%dM", (unsigned int)(data_partition_size / (1024 *1024)))) < 0) {
            ALOGE("Convert size fail\n");
            return -1;
        }

        ALOGI("Resize data partition to %s. Starting...\n", size);

        //Fork a new thread to run resize2fs
        pid = fork(); 
        switch(pid) {
            case -1:
                ALOGE("fork system call fail.\n");
                break;
            case 0: // child 
                execl("/system/bin/resize2fs", "resize2fs", "-f", DATA_PATH, size, NULL);
                ALOGE("execl fail.\n");
                _exit(127);
                break;
            default: //parent 
            // wait child process end
            while (pid != wait(&status));

            if (WIFEXITED(status)){
                child_return_val = WEXITSTATUS(status);
                if (child_return_val != 0) {
                    ALOGE("resize data partition fail. We will run e2fsck -pDf...");
                    resize_ret = 0;
                } else {
                    ALOGE("resize run ok. We will run e2fsck -pDf to fix journal size...");
                    resize_ret = 1;
                }
            }
                
        }
 
        //For a new thread to run e2fsck -pDf
        pid = fork(); 
        switch(pid) {
            case -1:
                ALOGE("fork system call fail.\n");
                break;
            case 0: // child 
                execl("/sbin/e2fsck", "e2fsck", "-pDf", DATA_PATH, NULL);
                ALOGE("execl fail.\n");
                _exit(127);
                break;
            default: //parent 
            // wait child process end
            while (pid != wait(&status));

            if (WIFEXITED(status)){
                child_return_val = WEXITSTATUS(status);
                if (child_return_val != 0 && child_return_val != 1){
                    ALOGE("Fail to correct filesystem...\n");
                    resize_ret = 0;
                } else {
                    ALOGE("successful to run e2fsck...\n");
                }
            }
                
        }

        ALOGI("successful to Risize data partition.\n");
        return 0;
/*
        if(1 == resize_ret) {
           //Set the system env. 
           if (set_system_env("yes") < 0) {
               ALOGE("Fail to set system env.\n");
               return -1;
           }
           ALOGI("successful to set system env.\n");
           return 0;
        } else {
            return -1;
        }
*/

    }
}

static int get_resize_stats(int *resize_stats)
{
	errcode_t	retval;
	ext2_filsys	fs;
	io_manager	io_ptr;
    unsigned long long data_partition_size = 0;

    io_ptr = unix_io_manager;
	retval = ext2fs_open2(DATA_PATH, NULL , 0,
			      0, 0, io_ptr, &fs);
	if (retval) {
		ALOGE("Couldn't find valid filesystem superblock.\n");
	    return -1;	
	}

    if((data_partition_size = get_block_size(DATA_PATH)) < 0) {
        ALOGE("Get data partition size fail.\n");
        ext2fs_close(fs);
        return -1;
    }
    data_partition_size -= RESERVE_SIZE;

    if((data_partition_size / fs->blocksize) == fs->super->s_blocks_count) {
        ALOGI("The size of data already meet the request.\n");
        *resize_stats = STATS_RESIZED;
    }
    ext2fs_close(fs);
    return 0;
}

static int set_system_env(const char *status){
    int fd;
    int ret;
    char item[255];

    if ((fd = open("/proc/lk_env", O_RDWR)) < 0) {
        ALOGE("Open lk_env file fail:%s.\n", (char*)strerror(errno));
        return -1;
    }

    if ((ret = sprintf(item, "resized_stats=%s",status)) < 0) {
        ALOGE("sprintf fail.\n");
        goto out_fail;
    }

    if (write(fd, item, sizeof(item)) < 0) {
        ALOGE("fail to write /proc/lk_evn\n");
        goto out_fail;
    }

    close(fd);
    return 0;

out_fail:
    close(fd);
    return -1;

}

static unsigned long long get_block_size(const char *path)
{
    int fd;
    int ret;
    unsigned long long block_size = 0;
    if ((fd = open(path, O_RDONLY)) < 0) {
        ALOGE("Open block fail:%s.\n", (char*)strerror(errno));
        return -1;
    }

	ret = ioctl(fd, BLKGETSIZE64, &block_size);
    close(fd);

	if (ret)
		return -1;

	return block_size;
}

static int process_system_env(int *resize_stats)
{
    int fd;
	struct env_ioctl en_ctl;
	char *name = NULL;
	char *value = NULL;
    
	memset(&en_ctl,0x00,sizeof(struct env_ioctl));

	name = (char*)calloc(1, BUF_MAX_LEN);
    if (NULL == name) {
        ALOGE("alloc memory for lk_env name fail\n");
        return -1;
    }
	value = (char*)calloc(1, BUF_MAX_LEN);
    if (NULL == value) {
        ALOGE("alloc memory for lk_env name fail\n");
        free(name);
        return -1;
    }

    if ((fd = open("/proc/lk_env", O_RDONLY)) < 0) {
        ALOGE("Open lk_env file fail:%s.\n", (char*)strerror(errno));
        goto out_free_memory;
    }

	memcpy(name, ITEM_NAME, strlen(ITEM_NAME)+1);	
	en_ctl.name = name;
	en_ctl.value = value;
	en_ctl.name_len = strlen(name)+1;
	en_ctl.value_len = BUF_MAX_LEN;

    if (ioctl(fd, ENV_READ, &en_ctl) < 0) {
        ALOGE("Ioctl for env read fail.\n");
        goto out_free_fd;
    }
	ALOGI("env read %s : %s\n", name, value);

    if (value != NULL) {
        if (!strcmp(value, "yes")) {
            *resize_stats = STATS_RESIZED;
            ALOGI("resize stats is %s\n", value);
        } else {
            ALOGI("resize stats is %s\n", value);
        }
    } else {
        ALOGI("reszied_stats is empty.\n");
    }
    close(fd);
    free(name);
    free(value);
    return 0;

out_free_fd:
    close(fd);
out_free_memory:
    free(name);
    free(value);
    return -1;
}

/*
static int process_system_env(int *resize_stats)
{
    int fd;
    char *buffer;
    unsigned int length = 0;

    buffer = (char*)calloc(1, LK_FILE_MAX_SIZE);
    if (NULL == buffer) {
        ALOGE("alloc memory for lk_env buffer fail\n");
        return -1;
    }

    if ((fd = open("/proc/lk_env", O_RDONLY)) < 0) {
        ALOGE("Open lk_env file fail:%s.\n", (char*)strerror(errno));
        goto out_free_memory;
    }

    if ((length = read(fd, buffer, LK_FILE_MAX_SIZE)) < 0) {
        ALOGE("Read lk_env fail:%s.\n", (char*)strerror(errno));
        goto out_free_fd; 
    } else if(0 == length) {
        ALOGI("lk_env is empty\n");
    } else {
        const char *delim = "=";
        char *save_ptr;
        char *item_name, *item_value;
        char *pitem = buffer;
        char *p;
        int i;

        for(i = 0, p = pitem; i < length; i++, p++) {
            if(*p == '\0' || p == buffer || i == (length -1)){
                if(!(item_name = strtok_r(pitem, delim, &save_ptr))) {
                    ALOGE("ERROR parsing system env item name.\n");
                    goto out_free_fd;
                }

                if (!strcmp(item_name, "resized_stats")) {
                    if(!(item_value = strtok_r(NULL, delim, &save_ptr))) {
                        ALOGE("ERROR parsing system env item value.\n");
                        goto out_free_fd;
                    }
                    if (!strcmp(item_value, "yes")) {
                        *resize_stats = STATS_RESIZED;
                        ALOGI("resize stats is %s\n", item_value);
                        break;
                    } else {
                        ALOGI("resize stats is %s\n", item_value);
                    }
                }

                pitem = p + 1;
            }
        }
    }

    close(fd);
    free(buffer);
    return 0;

out_free_fd:
    close(fd);
out_free_memory:
    free(buffer);
    return -1;
}
*/
