LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := eng

LOCAL_SRC_FILES:=       \
	audioregsetting.cpp
	
LOCAL_C_INCLUDES=       \
       $(LOCAL_PATH)/../../../../../frameworks/av/include/media \
       $(LOCAL_PATH)/../../frameworks-ext/av/include/media

LOCAL_SHARED_LIBRARIES := libcutils libutils libbinder libmedia libaudioflinger

LOCAL_MODULE:= audioregsetting

include $(BUILD_EXECUTABLE)

