LOCAL_PATH := $(my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libnvram
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_ARM_MODE := arm
LOCAL_PREBUILT_MODULE_FILE := $(TOPDIR)vendor/faea/mtk6589/proprietary/lib/libnvram.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../nvram/libnvram/
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES:= \
    $(LOCAL_PATH)/../nvram/libnvram \
    $(DEVICE_INCLUDE_PATH)

LOCAL_SRC_FILES := \
    AudioCustParam.cpp

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils 

LOCAL_LDFLAGS += -L$(TOPDIR)vendor/faea/mtk6589/proprietary/lib/ -lnvram

# Audio HD Record
LOCAL_CFLAGS += -DMTK_AUDIO_HD_REC_SUPPORT
# Audio HD Record
   
LOCAL_MODULE := libaudiocustparam

LOCAL_PRELINK_MODULE := false

LOCAL_ARM_MODE := arm

include $(BUILD_SHARED_LIBRARY)
