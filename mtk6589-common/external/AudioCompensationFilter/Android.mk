LOCAL_PATH := $(my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
AudioCompensationFilter.cpp \
AudioCompFltCustParam.cpp
#experimental

LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../nvram/libnvram $(DEVICE_INCLUDE_PATH)


LOCAL_PRELINK_MODULE := false 

LOCAL_SHARED_LIBRARIES := \
    libbessound_mtk \
    libnativehelper \
    libcutils \
    libutils 
	
LOCAL_LDFLAGS += -L$(TOPDIR)vendor/faea/mtk6589/proprietary/lib/ -lnvram

LOCAL_MODULE := libaudiocompensationfilter

LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
