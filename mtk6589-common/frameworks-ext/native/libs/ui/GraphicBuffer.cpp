/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "GraphicBuffer"

#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>

#include <cutils/xlog.h>

#include <ui/GraphicBuffer.h>
#include <ui/GraphicBufferAllocator.h>
#include <ui/GraphicBufferMapper.h>
#include <ui/PixelFormat.h>

#include <pixelflinger/pixelflinger.h>

#define LOCK_FOR_VA (GRALLOC_USAGE_SW_READ_RARELY | GRALLOC_USAGE_SW_WRITE_NEVER | GRALLOC_USAGE_HW_TEXTURE)

namespace android {

status_t GraphicBuffer::mapBuffer()
{
    void *addr;
    status_t result = lock(LOCK_FOR_VA, &addr);
    if (NO_ERROR == result)
    {
        ssize_t bpp = (format == HAL_PIXEL_FORMAT_I420) ?
                      2 : bytesPerPixel(format);
        if (bpp < 0)
        {
            XLOGW("Unable to mapBuffer, format(%d)", format);
            unlock();
            return bpp;
        }

        if (mva != 0 || msize != 0)
        {
            XLOGW("Already mapBuffer for w(%d) h(%d) bpp(%d) size(%d) va(%p) mva(0x%x)",
                   stride, height, (int)bpp, msize, addr, mva);
            unlock();
            return NO_ERROR;
        }

        int map_wdith = (format == HAL_PIXEL_FORMAT_I420) ? width : stride;

        unsigned int size = (int)(map_wdith * height * bpp);

        if (size <= 0)
        {
            XLOGW("Unable to mapBuffer, w(%d) h(%d) bpp(%d)", map_wdith, height, (int)bpp);
            unlock();
            return -1;
        }

        msize = size;

        GraphicBufferAllocator& allocator = GraphicBufferAllocator::get();
        result = allocator.map((unsigned int)addr, map_wdith, height, size, format, &mva);

        unlock();
    }

    return result;
}

status_t GraphicBuffer::unmapBuffer()
{
    void *addr;
    status_t result = lock(LOCK_FOR_VA, &addr);
    if (NO_ERROR == result)
    {
        ssize_t bpp = (format == HAL_PIXEL_FORMAT_I420) ?
                      2 : bytesPerPixel(format);
        if (bpp < 0)
        {
            XLOGW("Unable to unmapBuffer, format(%d)", format);
            unlock();
            return bpp;
        }

        int map_wdith = (format == HAL_PIXEL_FORMAT_I420) ? width : stride;

        unsigned int size = (int)(map_wdith * height * bpp);

        if (size <= 0)
        {
            XLOGW("Unable to unmapBuffer, w(%d) h(%d) bpp(%d)", map_wdith, height, (int)bpp);
            unlock();
            return -1;
        }

        if (msize != 0 && msize != size)
        {
            XLOGE("ERROR!! Invalid size change for unmapBuffer, map size(%d), current size(%d)",
                  msize, size);
            unlock();
            return -1;
        }

        GraphicBufferAllocator& allocator = GraphicBufferAllocator::get();
        result = allocator.unmap((unsigned int)addr, size, format, mva);

        unlock();
    }

    return result;
}

status_t GraphicBuffer::getIonFd(int *idx, int *num)
{
    return getBufferMapper().getIonFd(handle, idx, num);
}

void GraphicBuffer::setMva(unsigned int _mva)
{
#ifndef MTK_MMUMAP_SUPPORT
    mva = _mva;
#endif
}

// ---------------------------------------------------------------------------

}; // namespace android
