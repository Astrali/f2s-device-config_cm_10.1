package android.os;

import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.Printer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Date;
import java.text.SimpleDateFormat;

public class MessageLogger implements Printer {
    private final static String TAG = "MessageLogger";
    protected final static int MESSAGE_SIZE = 20 * 2;        // One for dispatching, one for finished
    protected final static int FLUSHOUT_SIZE = 1024*2;
    protected static boolean sEnableLooperLog;
    // To record all MessageLoggers in Process
    protected static HashMap<String, MessageLogger> sMap = new HashMap<String, MessageLogger>();
    protected String mName;

    private LinkedList mMessageHistoryRecord = new LinkedList();
    private LinkedList mMessageTimeRecord = new LinkedList();
    private LinkedList mNonSleepMessageTimeRecord = new LinkedList();

    private int mState = 0;
    private long mLastRecordKernelTime;            //Unir: Milli
    private long mNonSleepLastRecordKernelTime;    //Unit: Milli
    private long mLastRecordDateTime;              //Unir: Micro
    private long mWallStart;                //Unit:Micro
    private long mWallTime;                 //Unit:Micro
    private long mNonSleepWallStart;        //Unit:Milli
    private long mNonSleepWallTime;         //Unit:Milli
    private long mMsgCnt = 0;
    private String mLastRecord = null;

    public MessageLogger(String name) {
        mName = name;
    }

    public static MessageLogger newMessageLogger(boolean mValue, String name) {
        sEnableLooperLog = mValue;
        if (sMap.containsKey(name))
            sMap.remove(name);
        MessageLogger logger = new MessageLogger(name);
        sMap.put(name, logger);
        return logger;
    }

    public static void dumpMessageHistory(String name) {
        if (sMap == null)
            return;
        MessageLogger logger = sMap.get(name);
        if (logger != null)
            logger.dump();
    }

    public static void dumpAllMessageHistory() {
        if (sMap == null)
            return;
        Iterator<MessageLogger> it = sMap.values().iterator();
        while (it.hasNext())
            it.next().dump();
    }

    protected static void addTimeToList(LinkedList mList, long startTime, long durationTime) {
        mList.add(startTime);
        mList.add(durationTime);
        return;
    }

    public void println(String s) {
        synchronized (mMessageHistoryRecord) {
            mState++;
            int size = mMessageHistoryRecord.size();
            if (size > MESSAGE_SIZE) {
                mMessageHistoryRecord.removeFirst();
                mMessageTimeRecord.removeFirst();
                mNonSleepMessageTimeRecord.removeFirst();
            }
            s = "Msg#:" + mMsgCnt + " " + s;
            mMsgCnt++;

            mMessageHistoryRecord.add(s);
            mLastRecordKernelTime = SystemClock.elapsedRealtime();
            mNonSleepLastRecordKernelTime = SystemClock.uptimeMillis();
            mLastRecordDateTime = SystemClock.currentTimeMicro();
            if (mState%2 == 0) {
                mState = 0;
                mWallTime = SystemClock.currentTimeMicro() - mWallStart;
                mNonSleepWallTime = SystemClock.uptimeMillis() - mNonSleepWallStart;
                addTimeToList(mMessageTimeRecord, mWallStart, mWallTime);
                addTimeToList(mNonSleepMessageTimeRecord, mNonSleepWallStart, mNonSleepWallTime);
            } else {
                mWallStart = SystemClock.currentTimeMicro();
                mNonSleepWallStart = SystemClock.uptimeMillis();
            }

            if (sEnableLooperLog) {
                if (s.contains(">")) {
                    Log.d(TAG,"Debugging_MessageLogger: " + s + " start");
                } else {
                    Log.d(TAG,"Debugging_MessageLogger: " + s + " spent " + mWallTime / 1000 + "ms");
                }
            }
        }
    }

    protected static int sizeToIndex( int size) {
        return --size;
    }

    protected static void flushedOrNot(StringBuilder sb, boolean bl ) {
        if(sb.length() > FLUSHOUT_SIZE && !bl) {
            //Log.d(TAG, "After Flushed, Current Size Is:" + sb.length() + ",bool" + bl);
            sb.append("***Flushing, Current Size Is:" + sb.length() + ",bool:" + bl +"***TAIL\n");
            bl = true;
            Log.d(TAG, sb.toString());
            //Why New the one, not Clear the one? -> http://stackoverflow.com/questions/5192512/how-to-clear-empty-java-stringbuilder
            //Performance  is better for new object allocation.
            //sb = new StringBuilder(1024);
            sb.delete(0,sb.length());
        }
        else if(bl) {
            bl = false;
        }
    }

    public void dump() {
        synchronized (mMessageHistoryRecord) {
            StringBuilder history = new StringBuilder(1024);
            history.append("MSG HISTORY IN " + mName + " THREAD:\n");
            history.append("Current kernel time : " + SystemClock.elapsedRealtime() + "ms\n");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            //State = 1 means the current dispatching message has not been finished
            int sizeForMsgRecord = mMessageHistoryRecord == null ? 0 : mMessageHistoryRecord.size();
            if (mState == 1) {
                Date date = new Date((long)mLastRecordDateTime/1000);
                long spent = SystemClock.elapsedRealtime() - mLastRecordKernelTime;
                long nonSleepSpent = SystemClock.uptimeMillis()- mNonSleepLastRecordKernelTime;

                history.append("Last record : " + mMessageHistoryRecord.getLast());
                history.append("\n");
                history.append("Last record dispatching elapsedTime:" + spent + " ms/upTime:"+ nonSleepSpent +" ms\n");
                history.append("Last record dispatching time : " + simpleDateFormat.format(date));
                history.append("\n");
                sizeForMsgRecord --;
            }

            String msg = null;
            Long time = null;
            Long nonSleepTime = null;
            boolean flushed = false;
            for (;sizeForMsgRecord > 0; sizeForMsgRecord--) {
                msg = (String)mMessageHistoryRecord.get(sizeToIndex(sizeForMsgRecord));
                time = (Long)mMessageTimeRecord.get(sizeToIndex(sizeForMsgRecord));
                nonSleepTime = (Long)mNonSleepMessageTimeRecord.get(sizeToIndex(sizeForMsgRecord));
                if (msg.contains(">")) {
                    Date date = new Date((long)time.longValue()/1000);
                    history.append(msg + " from " + simpleDateFormat.format(date));
                    history.append("\n");
                } else {
                    history.append(msg + " elapsedTime:" + time/1000 + " ms/upTime:" + nonSleepTime +" ms");
                    history.append("\n");
                }
                flushedOrNot(history, flushed);
            }
            if(!flushed)
               Log.d(TAG, history.toString());
        }

        // Dump message queue
        Looper.getMainLooper().getQueue().dumpMessageQueue();
    }
}
