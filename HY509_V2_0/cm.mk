## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := HY509_V2_0

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/faea/HY509_V2_0/device_HY509_V2_0.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := HY509_V2_0
PRODUCT_NAME := cm_HY509_V2_0
PRODUCT_BRAND := faea
PRODUCT_MODEL := HY509_V2_0
PRODUCT_MANUFACTURER := faea
