USE_CAMERA_STUB := true

# inherit from the proprietary version
-include device/faea/mtk6589-common/BoardConfigCommon.mk

TARGET_PROVIDES_RILD := true
TARGET_PREBUILT_KERNEL := device/faea/HY509_V2_0/kernel

#TARGET_KERNEL_SOURCE := kernel/mediatek/mtk6589/
#TARGET_KERNEL_CONFIG := cyanogenmod_faeaf2_defconfig

BOARD_HAS_NO_SELECT_BUTTON := true
